package test

import (
	"time"
	"todo-api/internal/helpers"
	"todo-api/internal/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	salt, _           = helpers.GenerateSalt()
	hashedPassword, _ = helpers.HashPassword(`asdf1234`, salt)

	// UserData stores a test user
	UserData = &models.User{
		ID:           primitive.NewObjectID(),
		Email:        `test@test.com`,
		PasswordHash: hashedPassword,
		Salt:         salt,
		Active:       true,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}
)
