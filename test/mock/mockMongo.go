package mock

import (
	"context"
	"todo-api/internal/mongodb/wrapper"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoClient ...
type MongoClient struct {
	Databases map[string]*MongoDatabase
}

// Database ...
func (m *MongoClient) Database(name string) wrapper.Database {
	return m.Databases[name]
}

// MongoDatabase ...
type MongoDatabase struct {
	Collections map[string]*MongoCollection
}

// Collection ...
func (m *MongoDatabase) Collection(name string, opts ...*options.CollectionOptions) wrapper.Collection {
	return m.Collections[name]
}

// MongoCollection ...
type MongoCollection struct {
	Cursor       *MongoCursor
	SingleResult *MongoSingleResult
	UpdateResult *MongoUpdateResult
	InsertResult *mongo.InsertOneResult
	CountResult  int64
	Err          error
}

// CountDocuments ...
func (m *MongoCollection) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (i int64, err error) {
	return m.CountResult, m.Err
}

// Find ...
func (m *MongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (wrapper.Cursor, error) {
	return m.Cursor, m.Err
}

// FindOne ...
func (m *MongoCollection) FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) wrapper.SingleResult {
	return m.SingleResult
}

// UpdateOne ...
func (m *MongoCollection) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (wrapper.UpdateResult, error) {
	return m.UpdateResult, m.Err
}

// InsertOne ...
func (m *MongoCollection) InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (*mongo.InsertOneResult, error) {
	return m.InsertResult, m.Err
}

// MongoCursor ...
type MongoCursor struct {
	NextVal bool
	Err     error
	Data    interface{}
}

// All ...
func (m *MongoCursor) All(ctx context.Context, results interface{}) error {
	b, _ := bson.Marshal(m.Data)
	if err := bson.Unmarshal(b, results); err != nil {
		return err
	}
	return m.Err
}

// Next ...
func (m *MongoCursor) Next(ctx context.Context) bool {
	return m.NextVal
}

// Decode ...
func (m *MongoCursor) Decode(val interface{}) error {
	b, _ := bson.Marshal(m.Data)
	if err := bson.Unmarshal(b, val); err != nil {
		return err
	}
	return m.Err
}

// Close ...
func (m *MongoCursor) Close(ctx context.Context) error {
	return m.Err
}

// MongoSingleResult ...
type MongoSingleResult struct {
	Data interface{}
	Err  error
}

// Decode ...
func (m *MongoSingleResult) Decode(v interface{}) error {
	b, _ := bson.Marshal(m.Data)
	if err := bson.Unmarshal(b, v); err != nil {
		return err
	}
	return m.Err
}

// MongoUpdateResult ...
type MongoUpdateResult struct {
	Err error
}

// UnmarshalBSON ...
func (m *MongoUpdateResult) UnmarshalBSON(b []byte) error {
	return m.Err
}
