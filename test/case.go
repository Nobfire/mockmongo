package test

import "testing"

// Case represents a test case for unit testing
type Case struct {
	Name string
	F    func(*testing.T, ...interface{})
}
