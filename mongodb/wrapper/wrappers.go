package wrapper

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//-----------------------------------
type MongoClient struct {
	*mongo.Client
}

func (m MongoClient) Database(name string) Database {
	return &MongoDatabase{Database: m.Client.Database(name)}
}

//-----------------------------------
type MongoDatabase struct {
	*mongo.Database
}

func (m MongoDatabase) Collection(name string, opts ...*options.CollectionOptions) Collection {
	return &MongoCollection{Collection: m.Database.Collection(name)}
}

//-----------------------------------
type MongoCollection struct {
	*mongo.Collection
}

func (m MongoCollection) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error) {
	return m.Collection.CountDocuments(ctx, filter, opts...)
}

func (m MongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (Cursor, error) {
	cursor, err := m.Collection.Find(ctx, filter, opts...)
	return &MongoCursor{Cursor: cursor}, err
}

func (m MongoCollection) FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) SingleResult {
	singleResult := m.Collection.FindOne(ctx, filter, opts...)
	return &MongoSingleResult{SingleResult: singleResult}
}

func (m MongoCollection) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (UpdateResult, error) {
	updateResult, err := m.Collection.UpdateOne(ctx, filter, update, opts...)
	return &MongoUpdateResult{UpdateResult: updateResult}, err
}

func (m MongoCollection) InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (*mongo.InsertOneResult, error) {
	insertOneResult, err := m.Collection.InsertOne(ctx, document, opts...)
	return insertOneResult, err
}

//-----------------------------------
type MongoCursor struct {
	*mongo.Cursor
}

func (m MongoCursor) All(ctx context.Context, results interface{}) error {
	return m.Cursor.All(ctx, results)
}

func (m MongoCursor) Next(ctx context.Context) bool {
	return m.Cursor.Next(ctx)
}

func (m MongoCursor) Decode(val interface{}) error {
	return m.Cursor.Decode(val)
}

func (m MongoCursor) Close(ctx context.Context) error {
	return m.Cursor.Close(ctx)
}

//-----------------------------------
type MongoSingleResult struct {
	*mongo.SingleResult
}

func (m MongoSingleResult) Decode(v interface{}) error {
	return m.SingleResult.Decode(v)
}

//-----------------------------------
type MongoUpdateResult struct {
	*mongo.UpdateResult
}

func (m MongoUpdateResult) UnmarshalBSON(b []byte) error {
	return m.UpdateResult.UnmarshalBSON(b)
}
