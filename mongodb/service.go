package mongodb

import (
	"context"
	"time"
	"todo-api/internal/mongodb/wrapper"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Service represents a mongodb connection
type Service struct {
	database wrapper.Database
}

var service Service

const timeoutLimit time.Duration = 20

// New creates a real or fake mongodb connection
func (s *Service) New(uri, dbName string, mockClients ...wrapper.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeoutLimit*time.Second)
	defer cancel()

	if len(mockClients) > 0 {
		s.database = mockClients[0].Database(dbName)
	} else {
		client, err := mongo.Connect(
			ctx, options.Client().ApplyURI(uri),
		)

		if err != nil {
			return err
		}

		// Set client database
		database := client.Database(dbName)
		s.database = wrapper.MongoDatabase{Database: database}
	}

	return nil
}

// InitService creates a real or fake mongodb connection
func InitService(uri, dbName string, mockClients ...wrapper.Client) error {
	return service.New(uri, dbName, mockClients...)
}

// GetCollection returns a representation of a mongodb collection
func GetCollection(name string) wrapper.Collection {
	return service.database.Collection(name)
}
